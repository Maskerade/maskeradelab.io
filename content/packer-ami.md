Title: Using Packer to build AMI's 
Date: 2015-10-08 10:20
Category: DevOps
Image: packer-build.png
Client: Start Bootstrap
Client_Link: http://www.demo.com
Service: Dev Ops

This post runs through the steps I took to automate the create of Amazon AMI's using packer templates

As we started to move more & more of our different applications to AWS it became apparanet that we were going to need a more reliable & dependable method of creating & maintaining custom AMI's. 


As we started to move more of our different applications to AWS it became apparanet that we were going to need a more reliable & dependable method of creating & maintaining custom AMI's - this was all the more necessary as we are running a number of different AWS environments all setup slightly differently for varying reasons (i.e Development Environment, PCI/CDE Environment). It was easy to see that this had the potential to become a very time consuming process to maintain multiple differing base AMI's before even taking into account n

* GETS LATEST AMI ID
* STARTS BUILDER INSTANCE
* DELETES PACKER TEMPLATES ON BUILDER INSTANCE
* COPIES LATEST TEMPLATES TO BUILDER INSTANCE
* RUNS PACKER VIA SSH (passes in Script Name, passes in AMI ID)
* GET AMI ID & UUID and stores as artifacts(?)

* GET LATEST AMI ID
* RUN PACKER (use wrapper script)

* USES IAM POLICY ON BUILDER INSTANCE TO BE ABLE TO RUN VARIOUS AWS TASKS (Create Instance, etc)


