Title: Self Service Private Cloud
Date: 2015-10-08 10:20
Category: DevOps
Image: cloud.jpg
Client: Start Bootstrap
Client_Link: http://www.demo.com
Service: Dev Ops

This article details the steps taken to build a private could using ESXi (free license) & Ansible


This is the first article in a multi-part series of how we built our own 'garden shed grade' self-service private cloud for a test environment.

Whilst rifling through the bins one day one of our team members found some old 1U servers that looked to be in reasonable working order.... hmm so what if we paired these with some 'large' harddisks and some 'spare' memory we had....maybe we could use this as some kind of VM hosts.... and so the basis of a plan was hatched.

The first step was to get each server loaded up with the memory & disks (with the disks configured into a RAID set) then get the servers racked, cabled & patched into a dedicated test VLAN - once all that legwork was done it was time to move onto the software installs and configuration.

The decision was made to use VMWare ESXi 5.5 (Free Edition) as the Virtual Machine Host software - not a large amount of time was spent debating or deciding this, it was mainly nbased on a couple of simple factors, 1) it was low cost (ok free) 2) we already used ESXi in other areas so knew out way around the software and that it would work with our hardware / software stack.


The install of ESXi was pretty standard and straight forward - we used a bootable USB key with the ESXi installer on and went from here
