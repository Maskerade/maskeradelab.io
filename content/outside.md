Title: Outside in the sun
Date: 2016-08-29 10:20
Category:Article
Image: boi.png
Client: Start Bootstrap
Client_Link: http://www.demo.com
Service: Web Development



An h1 header
============

Paragraphs are separated by a blank line.

2nd paragraph. *Italic*, **bold**, and `monospace`. Itemized lists
look like:

  * this one
  * that one
  * the other one

Note that --- not considering the asterisk --- the actual text
content starts at 4-columns in.

> Block quotes are
> written like so.
>
> They can span multiple paragraphs,
> if you like.

Use 3 dashes for an em-dash. Use 2 dashes for ranges (ex., "it's all
in chapters 12--14"). Three dots ... will be converted to an ellipsis.
Unicode is supported. ☺

  :::yaml
  ---
  - hosts: localhost
    gather_facts: yes
    connection: local
    vars:
      var_aws_access_key: "{{ lookup('env','AWS_ACCESS_KEY') }}"
      var_aws_secret_key: "{{ lookup('env','AWS_SECRET_KEY') }}"
      asg_name: test-asg
      sp_cooldown: 120
      sp_scaling_adjustment_out: 1
      sp_scaling_adjustment_in: -1
      sp_min_adjustement_step: 1
    tasks:
      - name: Add scaleout-policy
        ec2_scaling_policy:
          state: present
          name: scaleout-policy
          adjustment_type: "ChangeInCapacity"
          asg_name: "{{ asg_name }}"
          scaling_adjustment: "{{ sp_scaling_adjustment_out }}"
          min_adjustment_step: "{{ sp_min_adjustement_step }}"
          cooldown: "{{ sp_cooldown }}"
      - name: Add scalein-policy
        ec2_scaling_policy:
          state: present
          name: scalein-policy
          adjustment_type: "ChangeInCapacity"
          asg_name: "{{ asg_name }}"
          scaling_adjustment: "{{ sp_scaling_adjustment_in }}"
          min_adjustment_step: "{{ sp_min_adjustement_step }}"
          cooldown: "{{ sp_cooldown }}"
