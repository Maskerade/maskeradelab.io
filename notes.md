Title: Notes and Things to do 
Date: 2010-12-03 10:20
Category:Article 
Image: placeholder.jpg
Client: Start Bootstrap
Client_Link: http://www.demo.com
Service: Web Development

# Notes & Things to do

* Using packer with Ansible as a provisioner - need to modify ansible scripts to run locally (what does this mean?)

* Some kind of Ansible tower...but free - how to run tasks from central server & monitor results.

* Ansible iptables rules - how to do this without stooping to UFW

* OPENLDAP service account 


 -----------
| 			|
| GO SERVER	|
| 			|
|			|
 -----------
 	  |
 	  |
 	  |
 	 \ /
 -----------
| 			|
| GO AGENT	|
| 			|
|			|
 -----------
 	 |
 	 | aws cli
 	 |
 -----------
| 			|
|    AWS    |
| 			|
|			|
 -----------



* Calls AWS to find BUILDER instance ID then power on the instance
* Using SSH calls Script on BUILDER instance (/opt/ami-foundry/cast-ami.sh)

Some ENV variables:

AMI_FOUNDRY_DIR = 
AMI_PACKERSCRIPT =
AMI_PACKERDIR = 
AMI_SRCH_RELEASE = 
AMI_SRCH_ARCH = 
AMI_SRCH_REGION = 
AMI_SRCH_VIRT = 
AMI_SRCH_VOL = 


SG_Automation_Policy & SG_Automation

